import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qrcodescanner/src/pages/home_page.dart';
import 'package:qrcodescanner/src/pages/map_page.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light
        .copyWith(statusBarColor: Colors.transparent));
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'QRReader',
        initialRoute: 'home',
        routes: {
          'home': (BuildContext context) => HomePage(),
          'map': (BuildContext context) => MapPage(),
        });
  }
}
