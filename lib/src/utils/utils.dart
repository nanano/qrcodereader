import 'package:flutter/cupertino.dart';
import 'package:qrcodescanner/src/models/scan_model.dart';
import 'package:url_launcher/url_launcher.dart';

openScan(ScanModel scan, [BuildContext context]) async {
  if (scan.type == 'http') {
    if (await canLaunch(scan.value)) {
      await launch(scan.value);
    } else {
      throw 'Could not launch ${scan.value}';
    }
  } else {
    if (context != null) {
      Navigator.pushNamed(context, 'map', arguments: scan);
    }
  }
}
