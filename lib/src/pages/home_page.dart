import 'package:flutter/material.dart';
import 'package:qrcodescanner/src/bloc/scans_bloc.dart';
import 'package:qrcodescanner/src/models/scan_model.dart';
import 'package:qrcodescanner/src/pages/adresses_page.dart';
import 'package:qrcodescanner/src/pages/maps_page.dart';
import 'package:barcode_scan/barcode_scan.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final scansBloc = new ScansBloc();
  int _currentSite = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QrScanner'),
        backgroundColor: Colors.indigo,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete_forever),
            color: Colors.white,
            onPressed: scansBloc.removeAllScans,
          )
        ],
      ),
      body: _callPage(_currentSite),
      bottomNavigationBar: _mainButtonNavigationBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: _mainFloatingButton(),
    );
  }

  Widget _mainButtonNavigationBar() {
    return BottomNavigationBar(
      selectedItemColor: Colors.indigo,
      unselectedItemColor: Colors.grey,
      backgroundColor: Colors.white,
      currentIndex: _currentSite,
      onTap: (index) {
        setState(() {
          _currentSite = index;
        });
      },
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.map), title: Text('Maps')),
        BottomNavigationBarItem(
            icon: Icon(Icons.settings_ethernet), title: Text('URIs')),
      ],
    );
  }

  Widget _callPage(int actualPage) {
    switch (actualPage) {
      case 0:
        return MapsPage();
        break;
      case 1:
        return AdressesPage();
        break;
      default:
        return MapsPage();
    }
  }

  Widget _mainFloatingButton() {
    return FloatingActionButton(
      backgroundColor: Colors.indigo,
      child: Icon(Icons.filter_center_focus, size: 40.0),
      onPressed: _scanQR,
    );
  }

  _scanQR() async {
    //https://censopet-432ef.web.app/#/
    //geo:4.728270980106464,-74.04233351191408
    String futureString = '';
    try {
      futureString = await BarcodeScanner.scan();
    } catch (e) {
      futureString = e.toString();
    }

    if (futureString != null) {
      final scan = ScanModel(value: futureString);
      scansBloc.addScan(scan);

      /*final scan2 =
          ScanModel(value: 'geo:4.728270980106464,-74.04233351191408');*/
    }
  }
}
