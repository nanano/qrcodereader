import 'package:flutter/material.dart';
import 'package:qrcodescanner/src/bloc/scans_bloc.dart';
import 'package:qrcodescanner/src/providers/db_provider.dart';
import 'package:qrcodescanner/src/utils/utils.dart' as utils;

class MapsPage extends StatelessWidget {
  final scansBloc = new ScansBloc();
  @override
  Widget build(BuildContext context) {
    scansBloc.obtainScansType('geo');
    return StreamBuilder(
      stream: scansBloc.scansStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        print(snapshot);
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        final List<ScanModel> scans = snapshot.data;
        if (scans.length == 0) {
          return Center(
            child: Text('No hay registros'),
          );
        }

        return ListView.builder(
          itemCount: scans.length,
          itemBuilder: (context, i) => Dismissible(
            direction: DismissDirection.endToStart,
            background: Container(
              color: Colors.red,
            ),
            key: UniqueKey(),
            onDismissed: (direction) =>
                scansBloc.removeScans(scans[i].id, scans[i].type),
            child: ListTile(
              onTap: () async {
                await utils.openScan(scans[i], context);
              },
              leading: Icon(
                Icons.assessment,
                color: Colors.indigo,
              ),
              title: Text(scans[i].value),
              trailing: Icon(Icons.keyboard_arrow_right),
            ),
          ),
        );
      },
    );
  }
}
