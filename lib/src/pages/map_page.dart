import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:qrcodescanner/src/models/scan_model.dart';

class MapPage extends StatelessWidget {
  final MapController mapController = new MapController();

  @override
  Widget build(BuildContext context) {
    final ScanModel scan = ModalRoute.of(context).settings.arguments;
    return Scaffold(
        appBar: AppBar(
          title: Text('Mapa'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.my_location),
              onPressed: () {
                mapController.move(scan.getLatLng(), 15);
              },
            )
          ],
        ),
        body: _createFlutterMap(scan));
  }

  Widget _createFlutterMap(ScanModel scan) {
    return new FlutterMap(
      mapController: mapController,
      options: new MapOptions(center: scan.getLatLng(), zoom: 15),
      layers: [_createMapView(), _createMarker(scan)],
    );
  }

  _createMapView() {
    return TileLayerOptions(
        urlTemplate: 'https://api.mapbox.com/v4/'
            '{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}',
        additionalOptions: {
          'accessToken':
              'pk.eyJ1IjoiYWZvbm5lZ3JhIiwiYSI6ImNrNGQ1MTB2NDB2OTgzb212NGJla3ZzZnQifQ.eVuE1pJ8gLBE-TPRBES9Fg',
          'id': 'mapbox.streets'
        });
  }

  _createMarker(ScanModel scan) {
    return MarkerLayerOptions(
      markers: [
        new Marker(
          width: 80.0,
          height: 80.0,
          point: scan.getLatLng(),
          builder: (ctx) => new Container(
            child: Image(image: AssetImage('assets/pin.png')),
          ),
        ),
      ],
    );
  }
}
