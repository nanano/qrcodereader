import 'dart:async';

import 'package:qrcodescanner/src/providers/db_provider.dart';

class ScansBloc {
  static final ScansBloc _scansBloc = new ScansBloc._private();

  factory ScansBloc() {
    return _scansBloc;
  }

  ScansBloc._private() {
    //Obtener scans de la base de datos
    obtainScans();
  }

  final _scansStreamController = StreamController<List<ScanModel>>.broadcast();

  Stream<List<ScanModel>> get scansStream => _scansStreamController.stream;

  void dispose() {
    _scansStreamController.close();
  }

  addScan(ScanModel scan) async {
    await DBProvider.db.newScan(scan);
    obtainScansType(scan.type);
  }

  obtainScans() async {
    _scansStreamController.sink.add(await DBProvider.db.getScans());
  }

  obtainScansType(String type) async {
    _scansStreamController.sink.add(await DBProvider.db.getScanType(type));
  }

  removeScans(int id, String type) async {
    await DBProvider.db.removeScan(id);
    obtainScansType(type);
  }

  removeAllScans() async {
    await DBProvider.db.removeAllScans();
    obtainScans();
  }
}
